// To parse this JSON data, do
//
//     final products = productsFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Products> productsFromJson(String str) => List<Products>.from(json.decode(str).map((x) => Products.fromJson(x)));

String productsToJson(List<Products> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Products {
  final String pid;
  final String name;
  final String price;
  final String description;

  Products({
    required this.pid,
    required this.name,
    required this.price,
    required this.description,
  });

  factory Products.fromJson(Map<String, dynamic> json) => Products(
    pid: json["pid"],
    name: json["name"],
    price: json["price"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "pid": pid,
    "name": name,
    "price": price,
    "description": description,
  };
}
